<?php


namespace calderawp\funCore\Api;

/**
 * Class Route
 *
 * Create WordPress REST API route
 *
 * @package calderawp\funCore\Api
 */
class Route implements \Caldera_Forms_API_Route
{
	/**
	 * Route base without namespace
	 *
	 * @since 0.0.1
	 *
	 * @var string
	 */
	protected  $base;

	/**
	 * Endpoints to add
	 *
	 * @since 0.0.1
	 *
	 * @var array
	 */
	protected $endpoints;

	/**
	 * Route constructor.
	 *
	 * @since 0.0.1
	 *
	 * @param string $base Route base without namespace
	 * @param array $endpoints Array of endpoints should have key of read and/or write. Read and write MUST be an Endpoint object.
	 */
	public function __construct( $base, array $endpoints )
	{
		$this->base = $base;
		$this->endpoints = $endpoints;
	}

	/** @inheritdoc */
	public function add_routes( $namespace ) {
		if( isset( $this->endpoints[ 'read' ] ) ) {
			register_rest_route($namespace, $this->base, $this->endpoints[ 'read' ]->toArray() );
		}

		if( isset( $this->endpoints[ 'write' ] ) ) {
			register_rest_route($namespace, $this->base, $this->endpoints[ 'write' ]->toArray() );
		}
	}
}