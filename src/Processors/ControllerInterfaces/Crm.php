<?php


namespace calderawp\funCore\Processors\ControllerInterfaces;


use calderawp\funCore\Processors\Data;

/**
 * Interface Crm
 *
 * CRM processor controllers MUST implement this
 *
 * @package calderawp\\{name}}
 */
interface Crm extends Controller
{
	/**
	 * Update via email
	 *
	 * @since 0.0.1
	 *
	 * @param Data $data
	 * @param $args
	 */
	public function sendViaEmail( Data $data, $args );

}