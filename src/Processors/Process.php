<?php


namespace calderawp\funCore\Processors;
use calderawp\calderaforms\pro\exceptions\Exception;
use calderawp\funCore\Container;
use calderawp\funCore\MagicallyHasContainer;

/**
 * Class Processor
 *
 * Technically these are the processor callbacks, but this is acting as dispatcher to actual processing
 *
 * @package calderawp\funCore\Processors
 */
class Process implements \Caldera_Forms_Processor_Interface_Process
{

	use MagicallyHasContainer;

	/**
	 * Array of events that can be subscribed to
	 *
	 * @since  0.0.1
	 *
	 * @var array
	 */
	protected $dispatchableAts;

	/**
	 * The callbacks to dispatch to
	 *
	 * @since  0.0.1
	 *
	 * @var Data
	 */
	private $dataObject;

	/**
	 * Processor entity
	 *
	 * @since  0.0.1
	 *
	 * @var Processor
	 */
	private  $processor;


	public function __call($name, $arguments)
	{
		switch ( $name ){
			case  'pre_process':
				$name = 'pre_processor';
				break;
			case 'process' :
				$name = 'processor';
				break;
			case 'post_process' :
				$name = 'post_processor';
				break;
		}

		if( method_exists( $this, $name ) ){
			call_user_func_array( [ $this, $name ], $arguments );
		}
	}

	/**
	 * Process constructor.
	 *
	 * @since  0.0.1
	 *
	 * @param Processor $processor Processor entity to be processed.
	 */
	public function __construct( Processor $processor )
	{
		$this->processor = $processor;
		$this->setDispatchesAts();

	}

	/** @inheritdoc */
	public function fields()
	{
		return $this->processor->getAdminFields();
	}

	/** @inheritdoc */
	public function pre_processor(array $config, array $form, $proccesid)
	{
		$form = Form::maybeFromArray( $form );
		$this
			->maybeSetDataObject( $config, $form, $proccesid )
			->dispatch( 'pre' );

		$errors = $this->getDataObject()->getErrors();
		if( ! empty( $errors ) ) {
			return $errors;
		}
	}


	/** @inheritdoc */
	public function processor(array $config, array $form, $proccesid)
	{
		$form = Form::maybeFromArray( $form );

		$this
			->maybeSetDataObject( $config, $form, $proccesid )
			->dispatch( 'process' );
		$errors = $this->getDataObject()->getErrors();

		if( ! empty( $errors ) ) {
			return $errors;
		}

	}


	/** @inheritdoc */
	public function post_processor(array $config, array $form, $proccesid)
	{
		$form = Form::maybeFromArray( $form );

		$this
			->maybeSetDataObject( $config, $form, $proccesid )
			->dispatch( 'post' );
	}


	/**
	 * Get callbacks object, lazy-loading as needed
	 *
	 * @since 0.0.1
	 *
	 * @param array $config
	 * @param Form $form
	 * @param string $proccesid
	 * @return $this;
	 */
	protected function maybeSetDataObject(array $config, Form $form, $proccesid )
	{
		if( empty( $this->dataObject ) )
		{
			$this->dataObject = new Data(
				new \Caldera_Forms_Processor_Get_Data( $config, $form->toArray(), $this->fields() ),
				$this,
				$form
			);
		}

		return $this;
	}

	/**
	 * Dispatch an event in processing
	 *
	 * @since 0.0.1
	 *
	 * @param string $att
	 * @param array $alsoWith Array of additional values to pass
	 */
	public function dispatch( $att, array  $alsoWith = [] )
	{
		if ( $this->isDispatchableAt( $att ) ) {
			/**
			 * The event will be dispatch to controller and dispatched through plugin event system, which by default uses WordPress plugin API.
			 */
			$filter = $this->dispatchFilterName($att);

			//Add filters
			$this->processor->routeAt( $att );

			//Apply filters -- if data object isset
			if( ! empty( $this->getDataObject() ) ){
				$this->getContainer()->applyFilters(
					$filter,
					$this->getDataObject(),
					$alsoWith
				);
			}
			return;


		}
	}

	/**
	 * Check if event to dispatch on is whitelisted or not
	 *
	 * @since 0.0.1
	 *
	 * @param string $att Event name
	 * @return bool
	 */
	protected function isDispatchableAt( $att)
	{
		$allowed = $this->dispatchesAt();
		return in_array( $att, $allowed );
	}
	/**
	 * @return Data
	 */
	protected function getDataObject()
	{
		return $this->dataObject;
	}

	/**
	 * Events this process dispatches at
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function dispatchesAt()
	{
		return $this->dispatchableAts;
	}

	/**
	 * Merges defaults and subclass provides whitelisted events to dispatch at
	 *
	 * @since 0.0.1
	 */
	private function setDispatchesAts()
	{
		$allowed = [
			'pre',
			'post',
			'process'
		];
		if (is_array($this->dispatchableAts)) {
			$allowed = array_merge($allowed, $this->dispatchableAts);
		}
		$this->dispatchableAts = $allowed;
	}

	/**
	 * Get name of filter we dispatch event on
	 *
	 * @since 0.0.1
	 *
	 * @param string $att
	 * @return string
	 */
	public function dispatchFilterName($att): string
	{
		$filter = 'caldera.fun-core.process.' . $att;
		return $filter;
	}


}