<?php


namespace calderawp\funCore\Container;


use Psr\Container\NotFoundExceptionInterface;

/**
 * Class NotFound
 *
 * Exception for item not found in container
 *
 * @package calderawp\funCore\Container
 */
class NotFound extends \Exception implements  NotFoundExceptionInterface
{

}