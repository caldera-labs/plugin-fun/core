<?php


namespace calderawp\funCore\Processors\Crm;


use calderawp\funCore\Processors\ControllerInterfaces\Crm;
use calderawp\funCore\Processors\ControllerInterfaces\Payment;
use calderawp\funCore\Processors\Data;
/**
 * Class Controller
 *
 * This is where the actual processing of the crm processor happens.
 *
 * @package calderawp\{[name}}
 */
class Controller implements Crm
{
	/** @inheritdoc */
	public function pre( Data $data )
	{

	}

	/** @inheritdoc */
	public function process( Data $data )
	{

	}

	/** @inheritdoc */
	public function post( Data $data )
	{

	}

	/** @inheritdoc */
	public function pay( Data $data)
	{

	}

	/** @inheritdoc */
	public function sendViaEmail( Data $data, $args )
	{

	}

}