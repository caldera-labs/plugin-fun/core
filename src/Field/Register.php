<?php

namespace calderawp\funCore\Field;


use calderawp\funCore\Service;


/**
 * Registers a field type via plugins API
 *
 * @since 0.0.1
 *
 * @package calderawp\\{[name}}
 */
class Register extends Service
{


	/**
	 * Field type
	 *
	 * @since 0.0.1
	 *
	 * @var Field
	 */
	protected $field;

	/**
	 * Identifier for field typ
	 *
	 * @since 0.0.1
	 *
	 * @var 0.0.1
	 */
	protected $identifier;

	/**
	 * Set Field object
	 *
	 * @since 0.0.1
	 *
	 * @param Field $field
	 *
	 * @return $this
	 */
	public function setField( Field  $field )
	{
		$this->field = $field;
		return $this;
	}

	/**
	 * Set field identifier
	 *
	 * @since 0.0.1
	 *
	 * @param $identifier
	 *
	 * @return $this
	 */
	public function setIdentifier( $identifier )
	{

		$this->identifier = $identifier;
		return $this;
	}

	/**
	 * Get field identifier
	 *
	 * @since 0.0.1
	 *
	 * @return string
	 */
	public function getIdentifier()
	{
		return ! empty( $this->identifier )
			? $this->identifier
			 : $this->getContainer()->getConfig()->name;
	}

	/**
	 * Register field type
	 *
	 * @use "caldera_forms_get_field_types" filter
	 *
	 * @since 0.0.1
	 *
	 * @param array $fields
	 *
	 * @return array
	 */
	public function registerField( $fields )
	{

		if( ! empty( $this->field->getConfig() ) ) {
			$fields[ $this->getIdentifier() ] = $this->field->getConfig();
		}

		return $fields;
	}

	/**
	 * Filter field attributes
	 *
	 * @since 0.1.0
	 *
	 * @uses "caldera_forms_field_attributes-$fieldType"
	 *
	 * @param $attrs
	 * @return mixed
	 */
	public function filterAttrs( $attrs )
	{

		$attrs[ 'type' ] = $this->field->getTypeAttribute();
		return $attrs;
	}

}