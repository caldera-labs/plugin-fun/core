<?php
namespace calderawp\funCore;

/**
 * Class Urls
 *
 * Dynamically generated, commonly used URLs by plugin
 *
 * @package calderawp\funCore;
 */
class Urls extends FactoryService
{

	/**
	 * Link to admin page.
	 *
	 * @since 0.0.1
	 *
	 * @param array $args Optional. Additional query args.
	 * @param string|bool $action Optional. If string query arg of that name, whose value is a nonce generated with that action will be added.
	 *
	 * @return string
	 */
	public  function adminUrl( array  $args = array( ), $action = false )
	{
		if( $action ){
			$args[ $action ] = wp_create_nonce( $action );
		}

		return add_query_arg(
			wp_parse_args( $args, array(
					'page' => $this->getContainer()->getConfig()->slug
				)
			), admin_url( 'admin.php' ) );
	}


	/**
	 * Get API URL
	 *
	 * This is complete URL
	 *
	 * @since 0.0.1
	 *
	 * @return string
	 */
	public function apiUrl()
	{
		return \Caldera_Forms_API_Util::url( $this->apiBase() );

	}

	/**
	 * Get API base
	 *
	 * This is end of URL cf-namespace/plugin-namespace
	 *
	 * @since 0.0.1
	 *
	 * @return string
	 */
	public function apiBase()
	{
		return sanitize_title_with_dashes($this->getContainer()->getConfig()->name ) . '/settings';
	}



}