<?php


namespace calderawp\funCore\Processors;


use Caldera_Forms_Processor_Get_Data;

/**
 * Class PaymentProcess
 *
 * Process dispatcher for payment processors
 *
 * @package calderawp\funCore
 */
class PaymentProcess extends Process implements \Caldera_Forms_Processor_Interface_Payment
{
	/** @internal   */
	protected $dispatchableAts = [
		'do_payment'
	];

	/** @inheritdoc */
	public function do_payment(array $config, array $form, $proccesid, Caldera_Forms_Processor_Get_Data $data_object)
	{
		$form = Form::maybeFromArray( $form );
		$this
			->maybeSetDataObject( $config, $form, $proccesid )
			->dispatch( 'do_payment'
		);

	}
}