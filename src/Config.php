<?php


namespace calderawp\funCore;

/**
 * Class Config
 *
 * Basic configuration details
 *
 * @package calderawp\funCore;
 */
class Config
{

	/**
	 * Version of plugin
	 *
	 * @since 0.0.1
	 *
	 * @var string
	 */
	protected $version;

	/**
	 * Slug of plugin
	 *
	 * @since 0.0.1
	 *
	 * @var string
	 */
	protected $slug;

	/**
	 * Name of plugin
	 *
	 * @since 0.0.1
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * Url for /dist dir
	 *
	 * @since 0.0.1
	 *
	 * @var string
	 */
	protected $distUrl;

	/**
	 * File path for /dist dir
	 *
	 * @since 0.0.1
	 *
	 * @var string
	 */
	protected $distDir;

	/**
	 * Create from stdClass object
	 *
	 * @param \stdClass $config
	 * @return static
	 */
	public static function factory( \stdClass $config )
	{
		$newInstance = new static( );
		foreach ( get_object_vars( $config ) as $var => $value ){
			$newInstance->$var = $value;
		}

		return $newInstance;
	}

	/** @inheritdoc */
	public function __get($name)
	{

		if( property_exists( $this, $name ) ){
			return $this->$name;
		}

	}

	/** @inheritdoc */
	public function __set($name, $value)
	{
		if( property_exists( $this, $name ) ){
			return $this->$name = $value;
		}
	}

}