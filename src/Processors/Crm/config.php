<?php
if( ! defined( 'ABSPATH' ) ){
	exit;
}

$processor = calderaFunCore()->getProcessor( 'fun-coreCrm' );
if( $processor ){
	$fields = $processor->getAdminFields();
	if( $fields ){
		echo \Caldera_Forms_Processor_UI::config_fields( $fields );
	}

}