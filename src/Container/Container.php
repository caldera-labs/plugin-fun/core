<?php


namespace calderawp\funCore\Container;


use calderawp\funCore\Config;
use Psr\Container\ContainerInterface;

/**
 * Class Container
 *
 * Base class for other containers to extend
 *
 * @package calderawp\funCore\Container
 */
abstract  class Container implements ContainerInterface
{

	/**
	 * Hidden Pimple Container
	 *
	 * Makes repository invisible, there is intentionally no access to this externally - this class must control container access.
	 *
	 * @since 0.0.1
	 *
	 * @var \Pimple\Container
	 */
	protected $container;

	/**
	 * Container constructor.
	 *
	 * @since 0.0.1
	 *
	 * @param Config $config Config option
	 *
	 */
	public function __construct( Config $config )
	{
		$this->container = new \Pimple\Container;
		$this->container->offsetSet( 'config', $config );
	}


	/** @inheritdoc */
	public function get( $id )
	{

		try{
			$this->has($id);
			return $this->container->offsetGet($id);
		}catch ( NotFound $e ){
			throw $e;
		}
	}

	/** @inheritdoc */
	public function has( $id )
	{
		if( $this->container->offsetExists( $id ) ){
			return true;
		}

		throw  new NotFound();
	}


}