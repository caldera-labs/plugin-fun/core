<?php

namespace calderawp\funCore\AdminPages;


/**
 * Class Scripts
 *
 * Handles CSS/JS for the admin page
 *
 * @package calderawp\funCore
 */
class Scripts
{
	/** @var string */
	protected $distUrl;

	/** @var string */
	protected $slug;

	/** @var string */
	protected $distDir;

	/**
	 * Scripts constructor.
	 *
	 * @param string $distUrl Url for /dist
	 * @param string $distDir Path for /dist
	 * @param string $slug Slug for script/css
	 * @param array $data Data to print to DOM
	 */
	public function __construct( $distUrl, $distDir, $slug, $data )
	{
		$this->distUrl = $distUrl;
		$this->slug = $slug;
		$this->distDir = $distDir;
		$this->data = $data;

	}


	/**
	 * Output webpack built UI
	 *
	 * @since 0.0.1
	 *
	 * @param null $context
	 * @param bool $enqueue_admin
	 * @return string
	 */
	public function webpack($context = null, $enqueue_admin = true)
	{
		$inline = \Caldera_Forms_Render_Util::create_cdata(
			'var FUN-CORE_ADMIN= ' . wp_json_encode( $this->data ) . ';'
		);

		if ($enqueue_admin) {
			wp_enqueue_style(
				\Caldera_Forms_Admin_Assets::slug('admin', false),
				\Caldera_Forms_Render_Assets::make_url('admin', false)
			);
		}

		ob_start();
		include $this->distDir . '/index.php';
		$prefix = 'cf-' . $this->name;
		$str = ob_get_clean();
		foreach (
			[
				 'styles',
				 'manifest',
				 'vendor',
				 'client'
			 ] as $thing
		) {
			$str = str_replace(
				'/' . $thing,
				$this->distUrl . $thing,
				$str);
		}

		if ($context) {

			$str = str_replace(
				$prefix,
				$prefix . $context,
				$str);
		}

		return $inline . str_replace([
				'<head>',
				'</head>'
			], '', $str);
	}

}