<?php


namespace calderawp\funCore;


use calderawp\funCore\Processors\Processor;

/**
 * Trait Controlled
 *
 * Allows for a processor to have all filter added it needs
 *
 * @package calderawp\win
 */
trait Controlled
{

	/**
	 * Attach controllers to a processor
	 *
	 * @since 0.0.1
	 *
	 * @param Processor $processor
	 */
	protected function control( Processor $processor )
	{
		$controller = $processor->getContainer()->getProcessorsFactory()->getProcessDispatcher($this->type, $processor);
		foreach ( $controller->dispatchesAt() as $event ){
			$controller->dispatch( $event );
		}


	}
}