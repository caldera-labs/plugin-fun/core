<?php


namespace calderawp\funCore\Api;

/**
 * Class Endpoint
 *
 * Create object for use in Route class that represents a REST API endpoint
 *
 * @package calderawp\funCore\Api
 */
class Endpoint
{

	/**
	 * Allowed request messages
	 *
	 * @since 0.0.1
	 *
	 * @var array|string
	 */
	protected $methods;

	/**
	 * Request Args
	 *
	 * @since 0.0.1
	 *
	 * @var array
	 */
	protected $requestArgs;

	/**
	 * Response callback
	 *
	 * @since 0.0.1
	 *
	 * @var array
	 */
	protected $responseCallback;

	/**
	 * Endpoint constructor.
	 *
	 * @since 0.0.1
	 *
	 * @param string|array|callable $requestCallback
	 * @param array $requestArgs
	 * @param array|string $methods
	 */
	public function __construct( $requestCallback = '', array  $requestArgs = [], array  $methods = [ 'GET' ] )
	{
		$this->responseCallback = $requestCallback;
		$this->requestArgs = is_string ( $requestArgs ) ? [ $requestArgs ] : $requestArgs;
		$this->methods = $methods;

	}

	/**
	 * Return as array that can be passed to  register_rest_route()
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function toArray()
	{
		$data = [
			'methods' => $this->methods,
			'args' => $this->requestArgs,
			'callback' => $this->responseCallback,
			'permission_callback' => [ $this, 'permissionsCallback' ],

		];

		return $data;
	}

	/**
	 * @param \WP_REST_Request $request
	 * @return bool
	 */
	public function permissionsCallback( \WP_REST_Request $request )
	{
		return current_user_can( \Caldera_Forms::get_manage_cap( 'admin' ) );
	}

}