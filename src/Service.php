<?php


namespace calderawp\funCore;

/**
 * Service to be register to Container
 *
 * All services registered to container MUST subclass this
 *
 * @package calderawp\funCore;
 */
abstract class Service
{

	/**
	 * Container instance
	 *
	 * @since 0.0.1
	 *
	 * @var Container
	 */
	private $container;

	/**
	 * Service constructor.
	 *
	 * @since 0.0.1
	 *
	 * @param Container $container
	 */
	public function __construct( Container $container )
	{
		$this->container = $container;
	}

	/**
	 * Get Container instance
	 *
	 * @since 0.0.1
	 *
	 * @return Container
	 */
	public function getContainer()
	{
		return $this->container;
	}

}