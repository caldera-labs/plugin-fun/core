<?php


namespace calderawp\funCore\Processors;
use calderawp\funCore\MagicallyHasContainer;

/**
 * Class Processor
 *
 * Entity describing processor
 */
abstract class Processor
{

	use MagicallyHasContainer;

	/**
	 * Processor identifier
	 *
	 * @since 0.0.1
	 *
	 * @var string
	 */
	protected $identifier;

	/**
	 * Callbacks for processing the processor
	 *
	 * @since 0.0.1
	 *
	 * @var Data
	 */
	protected $callbacks;

	/**
	 * Stores created process
	 *
	 * @var 0.0.1
	 */
	private $process;

	/**
	 * @var string
	 */
	protected $type = 'generic';


	/**
	 * Set processor identifier
	 *
	 * @since 0.0.1
	 *
	 * @param $identifier
	 *
	 * @return $this
	 */
	public function setIdentifier( $identifier )
	{

		$this->identifier = $identifier;
		return $this;
	}

	/**
	 * Routes  event
	 *
	 * @since 0.0.1
	 *
	 * @param string $att
	 */
	public function routeAt( $att )
	{
		$controller = $this->getController();
		$this->getContainer()->addFilter(
			$this->getDispatcher()->dispatchFilterName( $att ),
			[
				$controller,
				$att
			]


		);


	}

	/**
	 * Get controller object
	 *
	 * @since 0.0.1
	 *
	 * @return ControllerInterfaces\Controller
	 */
	public function getController()
	{
		return $this->getContainer()->getProcessorsFactory()->getProcessController( $this );
	}

	/**
	 * Get processor type
	 *
	 * @since 0.0.1
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * Create a process dispatch controller or something.
	 *
	 * @since 0.0.1
	 *
	 * @return Process
	 */
	public function toDispatcher()
	{
		return $this->getContainer()->getProcessorsFactory()->getProcessDispatcher(
			$this->type,
			$this
		);
	}

	/**
	 * Get "main" process for this processor
	 *
	 * @since 0.0.1
	 *
	 * @return Process
	 */
	public function getDispatcher()
	{
		if ( empty( $this->process ) ) {
			$this->process = $this->toDispatcher();
		}

		return $this->process;
	}

	/**
	 * Get processor identifier
	 *
	 * @since 0.0.1
	 *
	 * @return string
	 */
	public function getIdentifier()
	{
		return $this->identifier;
	}

	/**
	 * Get meta info for the plugin like author and suchs
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	abstract public function getMetaDetails();


	/**
	 * Get admin fields
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	abstract public function getAdminFields();


	/**
	 * Get processor
	 *
	 * @since 0.0.1
	 *
	 * @return string
	 */
	protected function processorName()
	{
		return $this->getContainer()->getConfig()->name . ' ' . $this->getType();
	}



}