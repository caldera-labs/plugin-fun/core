<?php
if( ! defined( 'ABSPATH' ) ){
	exit;
}

$processor = calderaFunCore()->getProcessor( 'fun-coreNewsletter' );
if( $processor ){
	$fields = $processor->getAdminFields();
	if( $fields ){
		echo \Caldera_Forms_Processor_UI::config_fields( $fields );
	}

}