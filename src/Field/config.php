<?php
if( ! defined( 'ABSPATH' ) ) {
	exit;
}

$adminFields = calderaFunCore()->getField()->getAdminFields();
if( ! empty( $adminFields ) ) {
	$string = Caldera_Forms_Processor_UI::config_fields($adminFields);
	echo $string;
}

