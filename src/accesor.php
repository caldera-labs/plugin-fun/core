<?php
/**
 * Get main instance of container for funCore
 *
 * @return Container
 */
function calderaFunCore(){
	static  $calderaFunCore;

	if( ! is_object( $calderaFunCore ) ){


		$config = Config::factory( (object) [
			'name' => 'core',
			'slug' => 'core'
		]);

		$calderaFunCore = new Container( $config );

		/**
		 * Runs when main instance of container is initialized
		 *
		 * @param Container $calderaFunCore instance of container when initialized
		 */
		do_action( 'caldera.funCore.init', $calderaFunCore );
	}

	return $calderaFunCore;

}