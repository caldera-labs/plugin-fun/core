<?php
/**
 * This file makes the front-end HTML
 *
 * You only need to change this if your field isn't an input
 */
?>
<?php echo $wrapper_before; ?>
	<?php echo $field_label; ?>
	<?php echo $field_before; ?>
		<?php echo Caldera_Forms_Field_Input::html( $field, $field_structure, $form ); ?>
		<?php echo $field_caption; ?>
	<?php echo $field_after; ?>
<?php echo $wrapper_after; ?>