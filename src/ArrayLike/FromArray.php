<?php

namespace calderawp\funCore;

/**
 * Trait FromArray
 *
 * Adds factory to convert arrays, if needed, to ArrayLike objects
 *
 * MUST only be used with ArrayLike subclasses.
 *
 * @package calderawp\name
 */
trait FromArray
{

	/**
	 * Items in collection
	 *
	 * @since 0.0.1
	 *
	 * @var array
	 */
	protected $items;


	/**
	 * FromArray constructor.
	 *
	 * @since 0.0.1
	 *
	 * @param array $items
	 */
	public function __construct( array  $items )
	{
		$this->items = $items;
	}

	/**
	 * Convert to array-like object if array
	 *
	 * @since 0.01
	 *
	 * @param array|ArrayLike $maybeArray Array or array-like object
	 *
	 * @return ArrayLike
	 */
	public static function maybeFromArray( $maybeArray )
	{
		if( is_array( $maybeArray ) )
		{
			return new ArrayLike();
		}

		//not an array
		return $maybeArray;

	}

}