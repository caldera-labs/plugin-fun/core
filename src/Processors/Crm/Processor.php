<?php


namespace calderawp\funCore\Processors\Crm;
use calderawp\funCore\Controlled;
use calderawp\funCore\Processors\Processor as ParentClass;
use calderawp\funCore\Processors\RegistersProcessor;

/**
 * Class Processor
 *
 * Defines a CRM processor entity
 *
 * @package calderawp\funCore
 */
class Processor extends ParentClass
{

	use RegistersProcessor;
	use Controlled;

	/** @inheirtdoc   */
	protected $type = 'crm';

	/** @inheirtdoc   */
	protected $identifier = 'fun-coreCrm';

	/**
	 * Processor constructor.
	 *
	 * @since 0.0.1
	 */
	public function __construct()
	{
		$this->registerProcessor( $this );
		$this->control( $this );
	}

	/**
	 * Get meta info for the plugin like author and suchs
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function getMetaDetails()
	{
		return [
			'name' 			=> $this->processorName(),
			'description' 	=> 'Caldera Forms add-on of industry ',
			'author' 		=> 'josh <josh@calderawp.com>',
			'author_url' 	=> 'https://calderaforms.com'
		];
	}
	/**
	 * Get admin fields
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function getAdminFields()
	{

		//SEE \Caldera_Forms_Processor_UI::config_field() for field tyeps
		return [
			[
				'type' => 'number',
				'required' => 'false',
				'magic' => true,
				'id' => 'size',
				'label' => __('Size', 'fun-core')
			],
			[
				'type' => 'dropdown',
				'options' => [
					'no' => __('No'),
					'yes' => __('Yes')
				],
				'id' => 'allow',
				'label' => __('allow', 'fun-core')
			]

		];
	}



}