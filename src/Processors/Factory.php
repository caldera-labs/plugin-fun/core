<?php


namespace calderawp\funCore\Processors;


use calderawp\funCore\FactoryService;
use calderawp\funCore\Processors\ControllerInterfaces\Controller;

class Factory extends FactoryService
{

	/**
	 * Processor cache
	 *
	 * @since 0.1.0
	 *
	 * @var array
	 */
	protected $processors;

	/**
	 * Processor controller cache
	 *
	 * @since 0.1.0
	 *
	 * @var array
	 */
	protected $processControllers;


	/**
	 * Get a process controller by tyep
	 *
	 * @since 0.0.1
	 *
	 * @param $type
	 * @param Processor $processor
	 * @return Process|CrmProcess|NewsletterProcess|PaymentProcess
	 */
	public function getProcessDispatcher($type, Processor $processor )
	{
		switch ( $type ){
			case 'payment':
				$dispatcher = new PaymentProcess( $processor );
				break;
			case 'crm':
				$dispatcher = new CrmProcess( $processor );
				break;
			case 'newsletter':
				$dispatcher = new NewsletterProcess( $processor );
				break;
			case 'validation':
			default :
				$dispatcher = new Process( $processor );
				break;

		}

		return $dispatcher;
	}

	/**
	 * Get controller object, by processor object
	 *
	 * @since 0.0.1
	 *
	 * @param Processor $processor Processor to get controller for.
	 * @return Controller
	 */
	public function getProcessController( Processor $processor )
	{
		if( ! isset( $this->processControllers[ $processor->getIdentifier() ] ) ){
			$this->processControllers[ $processor->getIdentifier() ] = $this->createController( $processor->getType() );
		}

		return $this->processControllers[ $processor->getIdentifier() ];
	}

	/**
	 * Controller object factory.
	 *
	 * @since 0.0.1
	 *
	 * @param  string $type Type of controller. Controller MUST exists
	 * @return Controller
	 */
	protected function createController( $type )
	{
		$controllerClass = __NAMESPACE__ . '\\' . ucfirst( $type ) . '\\Controller';
		return new $controllerClass;
	}

	/**
	 * Add processor to collection and register it for
	 *
	 * @since 0.0.1
	 *
	 * @param Processor $processor
	 */
	public function addProcessor( Processor $processor )
	{
		$this->maybeAddInitHook();
		$this->processors[ $processor->getIdentifier() ] = $processor;
		$this->processors[ $processor->getIdentifier() ] = new Register( $processor );

	}

	/**
	 * Add hooks to make all of the processors process during submission processing
	 *
	 * @since 0.0.1
	 */
	public function addHooks()
	{

		/** @var Processor $processor */
		foreach ( $this->processors as $processor ) {

			$this->getContainer()->addFilter(
				[
					'hook' => 'caldera_forms_get_form_processors',
					'callback' => [ $processor, 'registerProcessor']
				]
			);

			$this->getContainer()->addFilter(
				[
					'hook' => 'caldera_forms_get_form_templates',
					'callback' => [ $processor, 'formsTemplates']
				]
			);
		}
	}

	/**
	 *
	 */
	protected function maybeAddInitHook()
	{
		if (empty($this->processors)) {
			$this->getContainer()->addFilter(
				[
					'hook' => 'caldera_forms_pre_load_processors',
					'callback' => [$this, 'addHooks']
				]
			);
		}
	}
}