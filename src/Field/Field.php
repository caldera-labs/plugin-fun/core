<?php

namespace calderawp\funCore\Field;


class Field
{

	/**
	 * Get the value for type attribute
	 *
	 * @since 0.0.1
	 *
	 * @return string
	 */
	public function getTypeAttribute()
	{
		return 'text';
	}

	/**
	 * Get config for the field this plugin is for
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function getConfig()
	{
		return [
			//Change this to the name of your field
			'field'       => __( 'Special field', 'fun core' ),
			//Change this to the description of your field
			'description' => __( 'Description of special field', 'fun core' ),
			'file'        => plugin_dir_path( __FILE__ ) . 'front.php',
			//set category, custom categories are not allowed
			'category'    => __( 'Special', 'caldera-forms' ),
			//this function is called to save the field value
			//'handler'     => 'field_slug_handler',
			'setup'       => array(
				//ensure this is the right path for your field configuration file
				'template'      => plugin_dir_path( __FILE__ ) . 'config.php',
				//		//ensure this is the right path for your field admin preview file.
				'preview'       => plugin_dir_path( __FILE__ ) . 'preview.php',
				//Optional: Add any default settings -- caption|default|required etc. you do NOT want to be added.,
				'not_supported' => array(
				),
			),
			//Optional add any JavaScript files you want to be loaded when these field type is used
			//NOTE: Use full URL with correct protocol
			'scripts' => [],
			//Optional add any CSS files you want to be loaded when these field type is used
			//NOTE: Use full URL with correct protocol
			'styles' => []
		];
	}

	/**
	 * Get admin fields
	 *
	 * @since 0.0.1
	 *
	 * @return array
	 */
	public function getAdminFields()
	{

		//SEE \Caldera_Forms_Processor_UI::config_field() for field types
		return [
			 [
				'type' => 'number',
				'required' => 'false',
				'magic' => true,
				'id' => 'size',
				'label' => __( 'Size', 'fun core' )
			],
			 [
				'type' => 'dropdown',
				'options' => [
					'no' => __( 'No' ),
					'yes' => __( 'Yes' )
				],
				 'id' => 'allow',
				 'label' => __( 'allow', 'fun core' )
			]

		];
	}

}