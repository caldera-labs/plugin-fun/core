<?php


namespace calderawp\funCore\Processors\ControllerInterfaces;


use calderawp\funCore\Processors\Data;

/**
 * Payment Crm
 *
 * Payment processor controllers MUST implement this
 *
 * @package calderawp\\{name}}
 */
interface Payment extends Controller
{
	/**
	 * Process payment
	 *
	 * @since 0.0.1
	 *
	 * @param Data $data
	 */
	public function pay( Data $data );
}