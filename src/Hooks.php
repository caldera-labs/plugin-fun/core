<?php

namespace calderawp\funCore;


use NetRivet\WordPress\EventEmitter;

/**
 * Class Hooks
 *
 * Abstraction on top of WordPress plugins API
 *
 * All actions and filters MUST be registered here.
 *
 * @package calderawp{{name}}
 */
class Hooks
{

	/**
	 * Holds emitter to plugins API or whatever
	 *
	 * @since 0.0.1
	 *
	 * @var EventEmitter
	 */
	protected $eventsStysem;

	/**
	 * Hooks constructor.
	 *
	 * @since 0.0.1
	 *
	 * @param EventEmitter $eventsStysem
	 */
	public function __construct( EventEmitter $eventsStysem  )
	{
		$this->eventsStysem = $eventsStysem;
	}

	/**
	 * Add an action
	 *
	 * @since 0.0.1
	 *
	 * @param Hook $hook
	 *
	 * @return $this
	 */
	public function addAction( Hook $hook )
	{
		$this->eventsStysem->on(
			$hook->hook,
			$hook->callback,
			$hook->args,
			$hook->priority
		);
		return $this;
	}

	/**
	 * Add a filter
	 *
	 * @since 0.0.1
	 *
	 * @param Hook $hook Hook object for filter
	 *
	 * @return $this
	 */
	public function addFilter( Hook $hook )
	{
		$this->eventsStysem->filter(
			$hook->hook,
			$hook->callback,
			$hook->args,
			$hook->priority
		);
		return $this;
	}

	/**
	 * Apply filters
	 *
	 * @since 0.0.1
	 *
	 * @param Hook $hook Hook
	 * @param mixed $value Value for callback
	 * @param array $args Other args
	 * @return $this|mixed
	 */
	public function applyFilters( Hook $hook, $value, array $args  )
	{

		return $this->eventsStysem->applyFilters(
			$hook->hook,
			$value,
			$args
		);
	}

	/**
	 * Do action
	 *
	 * @since 0.0.1
	 *
	 * @param Hook $hook Hook
	 * @param array $args Other args
	 * @return $this|mixed
	 */
	public function doAction( Hook $hook, array $args )
	{
		return $this->eventsStysem->emit(
			$hook->hook,
			$args
		);
	}

}