<?php


namespace calderawp\funCore\Processors\ControllerInterfaces;


use calderawp\funCore\Processors\Data;
/**
 * Interface Newsletter
 *
 * Newsletter/ email marketing processor controllers MUST implement this
 *
 * @package calderawp\\{name}}
 */
interface Newsletter
{

	/**
	 * Subscribe someone to something so that they might be subscribed to this thing
	 *
	 * @since 0.0.1
	 *
	 * @param Data $data
	 * @param array $args
	 * @return mixed
	 */
	public function subscribe( Data $data, array  $args );
}